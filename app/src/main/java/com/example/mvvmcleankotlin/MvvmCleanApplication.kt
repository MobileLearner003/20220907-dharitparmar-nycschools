package com.example.mvvmcleankotlin

import android.app.*
import dagger.hilt.android.*
import timber.log.*
import timber.log.Timber.DebugTree

@HiltAndroidApp
class MvvmCleanApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
    }
}
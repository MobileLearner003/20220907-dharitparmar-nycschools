package com.example.mvvmcleankotlin.feature.schools.data

import com.example.mvvmcleankotlin.feature.schools.domain.SchoolRepository
import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject

class DefaultSchoolRepository @Inject constructor(
    private val retrofit: Retrofit
): SchoolRepository {

    //
    private val schoolApi: SchoolsApi by lazy { retrofit.create(SchoolsApi::class.java) }

    // todo(if I had time: I would return the )
    override suspend fun getSchools(): List<School> {
        return schoolApi.schools().also { Timber.d("list $it") }
    }

    override suspend fun getSatScoresBySchool(): List<SatScores> {
        return schoolApi.getNycHighSchoolsScores()
    }

}
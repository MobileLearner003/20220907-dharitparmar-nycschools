package com.example.mvvmcleankotlin.feature.schools.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.mvvmcleankotlin.databinding.FragmentSchoolDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsFragment: Fragment() {

    private lateinit var viewBinding: FragmentSchoolDetailsBinding
    private val viewModel by viewModels<SchoolsViewModel>()
    private val arg by navArgs<SchoolDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentSchoolDetailsBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
        }
        viewBinding.lifecycleOwner = viewLifecycleOwner
        // trigger data load
        viewModel.loadSchoolDetail(arg.school)

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // set navigation
        // load data
    }
}
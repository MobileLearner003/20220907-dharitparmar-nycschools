package com.example.mvvmcleankotlin.feature.schools.presentation

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmcleankotlin.feature.schools.data.School

@BindingAdapter("app:school_items")
fun setListItems(view: RecyclerView, list: List<School>) {
    (view.adapter as SchoolListAdapter).submitList(list)
}

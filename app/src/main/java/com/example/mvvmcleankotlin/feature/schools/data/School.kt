package com.example.mvvmcleankotlin.feature.schools.data

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class School(
    @Json(name = "dbn")
    val dbn: String,
    @Json(name = "school_name")
    val schoolName: String?,
    @Json(name = "overview_paragraph")
    val overviewParagraph: String?,
    @Json(name = "neighborhood")
    val neighborhood: String,
    @Json(name = "total_students")
    val totalStudents: String?,
    @Json(name = "city")
    val city: String?,
    @Json(name = "zip")
    val zip: String?,
    @Json(name = "primary_address_line_1")
    val address1: String?,
    @Json(name = "state_code")
    val state: String?
): Parcelable {

    companion object {
        // can be useful for test and default reuslt in case of failure
        val empty = School("", "","","","","","","","")
    }
}

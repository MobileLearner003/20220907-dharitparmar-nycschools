package com.example.mvvmcleankotlin.feature.schools.domain

import com.example.mvvmcleankotlin.feature.schools.data.*

interface SchoolRepository {
    suspend fun getSchools(): List<School>
    suspend fun getSatScoresBySchool(): List<SatScores>
}
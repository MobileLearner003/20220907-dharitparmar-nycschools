package com.example.mvvmcleankotlin.feature.schools.domain

import com.example.mvvmcleankotlin.core.interactor.AsyncUseCase
import com.example.mvvmcleankotlin.core.interactor.AsyncUseCase.None
import com.example.mvvmcleankotlin.feature.schools.data.School
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject


class GetSchools @Inject constructor(
    private val schoolRepository: SchoolRepository,
    coroutineDispatcher: CoroutineDispatcher
): AsyncUseCase<List<School>, None>(coroutineDispatcher) {

    override suspend fun execute(params: None): List<School> = schoolRepository.getSchools()
}
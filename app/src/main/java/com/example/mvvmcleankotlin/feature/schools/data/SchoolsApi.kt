package com.example.mvvmcleankotlin.feature.schools.data

import retrofit2.http.GET

interface SchoolsApi {

    companion object {
        private const val SCHOOLS = "resource/s3k6-pzi2.json"
        private const val SCHOOL_DETAIL = "resource/f9bf-2cp4.json"
    }

    @GET(value = SCHOOLS)
    suspend fun schools(): List<School>

    @GET(value = SCHOOL_DETAIL)
    suspend fun getNycHighSchoolsScores(): List<SatScores>
}
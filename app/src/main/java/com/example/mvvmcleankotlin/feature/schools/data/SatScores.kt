package com.example.mvvmcleankotlin.feature.schools.data

import com.squareup.moshi.*

@JsonClass(generateAdapter = true)
data class SatScores(
    @Json(name = "dbn")
    val dbn: String,
    @Json(name = "school_name")
    val schoolName: String,
    @Json(name = "sat_critical_reading_avg_score")
    val reading: String,
    @Json(name = "sat_math_avg_score")
    val math: String,
    @Json(name = "sat_writing_avg_score")
    val writing: String,
    @Json(name = "num_of_sat_test_takers")
    val avgTestTakers: String
)

package com.example.mvvmcleankotlin.feature.schools.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmcleankotlin.databinding.SchoolListItemBinding
import com.example.mvvmcleankotlin.feature.schools.data.School

class SchoolListAdapter(private val viewModel: SchoolsViewModel):
    ListAdapter<School, SchoolListAdapter.ItemViewHolder>(TaskDiffCallBack())
{

    internal var clickListener: (School) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val school = getItem(position)
        holder.bind(school, viewModel, clickListener)
    }

    class ItemViewHolder private constructor(
        val binding: SchoolListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(school: School, viewModel: SchoolsViewModel, clickListener: (School) -> Unit) {
            binding.viewmodel = viewModel
            binding.item = school
            binding.root.setOnClickListener {
                clickListener(school)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun create(parent: ViewGroup): ItemViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = SchoolListItemBinding.inflate(inflater, parent, false)
                return ItemViewHolder(binding)
            }
        }
    }
}

class TaskDiffCallBack: DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }
}
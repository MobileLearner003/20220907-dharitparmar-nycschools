package com.example.mvvmcleankotlin.feature.schools.presentation

import androidx.lifecycle.*
import com.example.mvvmcleankotlin.core.interactor.AsyncUseCase.None
import com.example.mvvmcleankotlin.feature.schools.domain.GetSchools
import com.example.mvvmcleankotlin.feature.schools.data.School
import com.example.mvvmcleankotlin.feature.schools.domain.GetSatScoreBySchool
import com.example.mvvmcleankotlin.feature.schools.domain.GetSatScoreBySchool.SatScoreResult
import com.example.mvvmcleankotlin.feature.schools.domain.GetSatScoreBySchool.SatScoreResult.Found
import com.example.mvvmcleankotlin.feature.schools.domain.GetSatScoreBySchool.SatScoreResult.NotFound
import com.example.mvvmcleankotlin.feature.schools.data.SatScores
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.Result.Companion

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val getSchools: GetSchools,
    private val getSatScoreBySchool: GetSatScoreBySchool
): ViewModel() {

    private var _schools = MutableLiveData<List<School>>().apply { value = emptyList() }
    val schools: LiveData<List<School>> = _schools

    // This LiveData depends on another so we can use a transformation.
    val empty: LiveData<Boolean> = Transformations.map(_schools) {
        it.isEmpty()
    }

    private var _schoolDetail= MutableLiveData<SchoolDetailsView>()
    var schoolDetails: LiveData<SchoolDetailsView> = _schoolDetail

    init {
        loadSchools()
    }

    private fun loadSchools() = viewModelScope.launch { _schools.value = getSchools(None()) }

    fun loadSchoolDetail(school: School) {
        viewModelScope.launch {
            handleSchoolDetail(school, getSatScoreBySchool(school.dbn))
        }
    }
    
    private fun handleSchoolDetail(school: School, scores: SatScoreResult<SatScores>) {

        _schoolDetail.value =  when (scores) {
            is Found ->
                scores.score.let {
                    SchoolDetailsView(
                        it.schoolName,
                        school.overviewParagraph ?: "",
                        it.math,
                        it.reading,
                        it.writing,
                        it.avgTestTakers
                    )
                }
            is NotFound ->
                // just for brevity return dummy value
                // in production show something meaningful to user
                SchoolDetailsView(
                    school.schoolName ?: "Name not available",
                    school.overviewParagraph ?: "Info Not Found",
                    "Record Not Found",
                    "Record Not Found",
                    "Record Not Found",
                    "Record Not Found"
                )
        }
    }
    
    // in prod scale as needed
    data class SchoolDetailsView(
        val name: String,
        val summary: String,
        val mathScore: String,
        val readingScore: String,
        val writingScore: String,
        val avgTestTakers: String
    )

}
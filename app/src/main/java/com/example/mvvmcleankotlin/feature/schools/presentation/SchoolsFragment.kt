package com.example.mvvmcleankotlin.feature.schools.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.mvvmcleankotlin.core.platform.*
import com.example.mvvmcleankotlin.databinding.FragmentSchoolsBinding
import com.example.mvvmcleankotlin.feature.schools.data.School
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class SchoolsFragment: Fragment() {

    private lateinit var itemAdapter: SchoolListAdapter
    private lateinit var viewDataBinding: FragmentSchoolsBinding

    private val viewModel by viewModels<SchoolsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        viewDataBinding = FragmentSchoolsBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
        }

        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        // create adapter
        setupListAdapter()
    }

    private fun setupListAdapter() {
        viewDataBinding.viewmodel?.also {
            itemAdapter = SchoolListAdapter(it)
            viewDataBinding.itemList.adapter = itemAdapter
            itemAdapter.clickListener = { school ->
                openSchoolDetail(school)
            }
        } ?: Timber.d("view model null")
    }

    private fun openSchoolDetail(school: School) {
        SchoolsFragmentDirections.actionSchoolsFragmentToSchoolDetailsFragment(school).also {
            findNavController().navigate(it)
        }
    }
}
package com.example.mvvmcleankotlin.feature.schools.domain

import com.example.mvvmcleankotlin.core.interactor.AsyncUseCase
import com.example.mvvmcleankotlin.feature.schools.data.*
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import com.example.mvvmcleankotlin.feature.schools.domain.GetSatScoreBySchool.SatScoreResult

class GetSatScoreBySchool @Inject constructor(
    dispatcher: CoroutineDispatcher,
    private val repository: SchoolRepository
): AsyncUseCase<SatScoreResult<SatScores>, String>(dispatcher) {

    override suspend fun execute(params: String): SatScoreResult<SatScores> {
        val satScore = repository.getSatScoresBySchool().find {
            it.dbn == params
        }
        return if (satScore != null) {
            SatScoreResult.Found(satScore)
        } else SatScoreResult.NotFound
    }

    sealed class SatScoreResult<out T> {
        data class Found(val score: SatScores): SatScoreResult<SatScores>()
        object NotFound: SatScoreResult<Nothing>()
    }
}
package com.example.mvvmcleankotlin

sealed class ResultType<out R> {

    data class Success<out T>(val data: T) : ResultType<T>()
    data class Error(val exception: Exception) : ResultType<Nothing>()
}
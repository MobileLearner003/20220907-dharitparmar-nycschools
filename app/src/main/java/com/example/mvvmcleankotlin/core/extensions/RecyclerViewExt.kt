package com.example.mvvmcleankotlin.core

import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("app:divider")
fun setDivider(view: RecyclerView, @DrawableRes drawableRes: Int) {
    val divider = DividerItemDecoration(
        view.context,
        DividerItemDecoration.VERTICAL
    )
    val drawable = ContextCompat.getDrawable(
        view.context,
        drawableRes
    )
    drawable?.let {
        divider.setDrawable(it)
        view.addItemDecoration(divider)
    }
}

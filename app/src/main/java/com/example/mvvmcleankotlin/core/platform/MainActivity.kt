package com.example.mvvmcleankotlin.core.platform

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvvmcleankotlin.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }
}
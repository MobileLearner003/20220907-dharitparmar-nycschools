package com.example.mvvmcleankotlin.core.interactor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

abstract class AsyncUseCase<out Type, in Params>(
    private val dispatcher: CoroutineDispatcher
) where Type: Any, Params: Any {

    // override this to encapsulate the code to be executed
    abstract suspend fun execute(params: Params): Type


    //Given more time: this could be wrapped inside try catch to handle an exception
    suspend operator fun invoke(params: Params): Type =
        withContext(dispatcher) {
            execute(params)
        }

    // represents a clear understanding - when a use-case have no input
    class None
}
package com.example.mvvmcleankotlin.feature.schools.domain

import com.example.mvvmcleankotlin.*
import com.example.mvvmcleankotlin.core.interactor.AsyncUseCase.None
import com.example.mvvmcleankotlin.feature.schools.data.*
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.*
import org.junit.*


// Testing a business logic
class GetSchoolsTest : UnitTest() {

    // subject under test
    private lateinit var getSchools: GetSchools

    @MockK
    private lateinit var schoolRepository: SchoolRepository

    @Before
    fun setUp() {
        getSchools = GetSchools(schoolRepository, Dispatchers.Unconfined)
        coEvery { schoolRepository.getSchools() } returns (listOf(School.empty))
    }

    // bare minimum test
    @Test
    fun `should get data from repository`() {
        runBlocking { getSchools.execute(None()) }

        coVerify(exactly = 1) { schoolRepository.getSchools() }
    }

}
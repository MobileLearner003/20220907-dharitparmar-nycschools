package com.example.mvvmcleankotlin

import io.mockk.*
import org.junit.*
import org.junit.rules.TestRule

abstract class UnitTest {

    @JvmField
    @Rule
    val injectMocks = TestRule { statement, _ ->
        MockKAnnotations.init(this@UnitTest, relaxUnitFun = true)
        statement
    }
}
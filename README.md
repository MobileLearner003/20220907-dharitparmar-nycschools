# NYC Schools
This app fetches list of NYC School and shows them as a list. When selecting a specific school from the list, it navigates to the school details screen and shows SAT scores.

## Architecture

The general principle is to use a basic 3 tiers architecture(data, diomain and presentation) with MVVM pattern. That is very easy to understand and many people are familir with it. So we will break down our solution into layers in order to respect the dependency rule (where dependencies flow in one direction).

## App architecture components:
- ViewModel
- LiveData
- Data Binding
- Navigation
- Retrofit
- Hilt DI
- Kotlin
- coroutine
- mockk
